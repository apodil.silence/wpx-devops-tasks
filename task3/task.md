# Task 3

## Install docker on the VM:

- Install docker via apt command:
`apt install docker.io`
- Test if it's wokring:
`docker run hello-world`

## Setup git:

- Configure user and email:
`git config --global user.name "Stefan Petrov"`
`git config --global user.email "apodil.silence@gmail.com"`
- Add remote repo:
`git remote add origin https://gitlab.com/m0sh1x2/fnx-devops-interview-tasks.git`
- Pull files from the remote repo:
`git pull origin main`

## Build container image

- Build the docker image + tag option for the image name
`docker build --tag wordpress .`
- Run the image to test it
`docker run wordpress`
- Push image to registry
`docker push krax0x/wordpress`

## Scan image with vulnerability tools:

- Docker Bench for Security
`git clone https://github.com/docker/docker-bench-security.git
cd docker-bench-security
sh docker-bench-security.sh`

- Dive
`wget https://github.com/wagoodman/dive/releases/download/v0.9.2/dive_0.9.2_linux_amd64.deb
apt install ./dive_0.9.2_linux_amd64.deb
dive wp`

- Trivy
`sudo apt-get install wget apt-transport-https gnupg lsb-release
wget -qO - https://aquasecurity.github.io/trivy-repo/deb/public.key | sudo apt-key add -
echo deb https://aquasecurity.github.io/trivy-repo/deb $(lsb_release -sc) main | sudo tee -a /etc/apt/sources.list.d/trivy.list
sudo apt-get update
sudo apt-get install trivy
trivy i wp`

# Bonus: Create a docker-compose configuration that will run the Whole WordPress installation + MariaDB/MySQL

- Create compose file
`vim docker-compose.yml`
- Install docker compose
`apt install docker-compose`
- Run the file
`docker-compose up -d`

