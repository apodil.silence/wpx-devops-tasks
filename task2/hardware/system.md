# dmidecode 3.2
Getting SMBIOS data from sysfs.
SMBIOS 2.8 present.

Handle 0x0100, DMI type 1, 27 bytes
System Information
	Manufacturer: Hetzner
	Product Name: vServer
	Version: 20171111
	Serial Number: 19423107
	UUID: 3c40d70d-2880-4261-b22b-861054c0f4c3
	Wake-up Type: Power Switch
	SKU Number: Not Specified
	Family: Hetzner_vServer

