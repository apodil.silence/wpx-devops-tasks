  UNIT                                                  LOAD      ACTIVE   SUB     DESCRIPTION                                                                  
  acpid.service                                         loaded    inactive dead    ACPI event daemon                                                            
  apparmor.service                                      loaded    active   exited  Load AppArmor profiles                                                       
  apport-autoreport.service                             loaded    inactive dead    Process error reports when automatic reporting is enabled                    
  apport.service                                        loaded    active   exited  LSB: automatic crash report generation                                       
  apt-daily-upgrade.service                             loaded    inactive dead    Daily apt upgrade and clean activities                                       
  apt-daily.service                                     loaded    inactive dead    Daily apt download activities                                                
  atd.service                                           loaded    active   running Deferred execution scheduler                                                 
● auditd.service                                        not-found inactive dead    auditd.service                                                               
  blk-availability.service                              loaded    active   exited  Availability of block devices                                                
  cloud-config.service                                  loaded    active   exited  Apply the settings specified in cloud-config                                 
  cloud-final.service                                   loaded    active   exited  Execute cloud user/final scripts                                             
  cloud-init-hotplugd.service                           loaded    inactive dead    cloud-init hotplug hook daemon                                               
  cloud-init-local.service                              loaded    active   exited  Initial cloud-init job (pre-networking)                                      
  cloud-init.service                                    loaded    active   exited  Initial cloud-init job (metadata service crawler)                            
● connman.service                                       not-found inactive dead    connman.service                                                              
● console-screen.service                                not-found inactive dead    console-screen.service                                                       
  console-setup.service                                 loaded    active   exited  Set console font and keymap                                                  
  cron.service                                          loaded    active   running Regular background program processing daemon                                 
  dbus.service                                          loaded    active   running D-Bus System Message Bus                                                     
● display-manager.service                               not-found inactive dead    display-manager.service                                                      
  dm-event.service                                      loaded    inactive dead    Device-mapper event daemon                                                   
  dmesg.service                                         loaded    inactive dead    Save initial kernel messages after boot                                      
  e2scrub_all.service                                   loaded    inactive dead    Online ext4 Metadata Check for All Filesystems                               
  e2scrub_reap.service                                  loaded    inactive dead    Remove Stale Online ext4 Metadata Check Snapshots                            
  emergency.service                                     loaded    inactive dead    Emergency Shell                                                              
● fcoe.service                                          not-found inactive dead    fcoe.service                                                                 
  finalrd.service                                       loaded    active   exited  Create final runtime dir for shutdown pivot root                             
  fstrim.service                                        loaded    inactive dead    Discard unused blocks on filesystems from /etc/fstab                         
  fwupd-refresh.service                                 loaded    inactive dead    Refresh fwupd metadata and update motd                                       
  fwupd.service                                         loaded    active   running Firmware update daemon                                                       
  getty-static.service                                  loaded    inactive dead    getty on tty2-tty6 if dbus and logind are not available                      
  getty@tty1.service                                    loaded    active   running Getty on tty1                                                                
  grub-common.service                                   loaded    inactive dead    Record successful boot for GRUB                                              
  grub-initrd-fallback.service                          loaded    inactive dead    GRUB failed boot detection                                                   
  hc-net-scan.service                                   loaded    inactive dead    Finds and configures Hetzner Cloud private network interfaces                
● hv_kvp_daemon.service                                 not-found inactive dead    hv_kvp_daemon.service                                                        
  irqbalance.service                                    loaded    active   running irqbalance daemon                                                            
● iscsi-shutdown.service                                not-found inactive dead    iscsi-shutdown.service                                                       
  iscsid.service                                        loaded    inactive dead    iSCSI initiator daemon (iscsid)                                              
● kbd.service                                           not-found inactive dead    kbd.service                                                                  
  keyboard-setup.service                                loaded    active   exited  Set the console keyboard layout                                              
  kmod-static-nodes.service                             loaded    active   exited  Create list of static device nodes for the current kernel                    
  logrotate.service                                     loaded    inactive dead    Rotate log files                                                             
● lvm2-activation-early.service                         not-found inactive dead    lvm2-activation-early.service                                                
● lvm2-activation.service                               not-found inactive dead    lvm2-activation.service                                                      
  lvm2-lvmpolld.service                                 loaded    inactive dead    LVM2 poll daemon                                                             
  lvm2-monitor.service                                  loaded    active   exited  Monitoring of LVM2 mirrors, snapshots etc. using dmeventd or progress polling
  lxd-agent-9p.service                                  loaded    inactive dead    LXD - agent - 9p mount                                                       
  lxd-agent.service                                     loaded    inactive dead    LXD - agent                                                                  
  man-db.service                                        loaded    inactive dead    Daily man-db regeneration                                                    
  ModemManager.service                                  loaded    active   running Modem Manager                                                                
  modprobe@drm.service                                  loaded    inactive dead    Load Kernel Module drm                                                       
  motd-news.service                                     loaded    inactive dead    Message of the Day                                                           
  multipathd.service                                    loaded    active   running Device-Mapper Multipath Device Controller                                    
  netplan-ovs-cleanup.service                           loaded    inactive dead    OpenVSwitch configuration for cleanup                                        
  networkd-dispatcher.service                           loaded    active   running Dispatcher daemon for systemd-networkd                                       
● networking.service                                    not-found inactive dead    networking.service                                                           
● NetworkManager.service                                not-found inactive dead    NetworkManager.service                                                       
  ondemand.service                                      loaded    inactive dead    Set the CPU Frequency Scaling governor                                       
  open-iscsi.service                                    loaded    inactive dead    Login to default iSCSI targets                                               
  open-vm-tools.service                                 loaded    inactive dead    Service for virtual machines hosted on VMware                                
● ovsdb-server.service                                  not-found inactive dead    ovsdb-server.service                                                         
  plymouth-quit-wait.service                            loaded    inactive dead    Hold until boot process finishes up                                          
  plymouth-quit.service                                 loaded    inactive dead    Terminate Plymouth Boot Screen                                               
  plymouth-read-write.service                           loaded    inactive dead    Tell Plymouth To Write Out Runtime Data                                      
  plymouth-start.service                                loaded    inactive dead    Show Plymouth Boot Screen                                                    
  polkit.service                                        loaded    active   running Authorization Manager                                                        
  pollinate.service                                     loaded    inactive dead    Pollinate to seed the pseudo random number generator                         
  qemu-guest-agent.service                              loaded    active   running QEMU Guest Agent                                                             
● rbdmap.service                                        not-found inactive dead    rbdmap.service                                                               
  rc-local.service                                      loaded    inactive dead    /etc/rc.local Compatibility                                                  
  rescue.service                                        loaded    inactive dead    Rescue Shell                                                                 
  rsync.service                                         loaded    inactive dead    fast remote file copy program daemon                                         
  rsyslog.service                                       loaded    active   running System Logging Service                                                       
  secureboot-db.service                                 loaded    inactive dead    Secure Boot updates for DB and DBX                                           
  serial-getty@ttyS0.service                            loaded    active   running Serial Getty on ttyS0                                                        
  setvtrgb.service                                      loaded    active   exited  Set console scheme                                                           
● snapd.seeded.service                                  not-found inactive dead    snapd.seeded.service                                                         
  ssh.service                                           loaded    active   running OpenBSD Secure Shell server                                                  
● sshd-keygen.service                                   not-found inactive dead    sshd-keygen.service                                                          
  systemd-ask-password-console.service                  loaded    inactive dead    Dispatch Password Requests to Console                                        
  systemd-ask-password-plymouth.service                 loaded    inactive dead    Forward Password Requests to Plymouth                                        
  systemd-ask-password-wall.service                     loaded    inactive dead    Forward Password Requests to Wall                                            
  systemd-binfmt.service                                loaded    inactive dead    Set Up Additional Binary Formats                                             
  systemd-boot-system-token.service                     loaded    inactive dead    Store a System Token in an EFI Variable                                      
  systemd-fsck-root.service                             loaded    inactive dead    File System Check on Root Device                                             
  systemd-fsck@dev-disk-by\x2duuid-39DC\x2dD48A.service loaded    active   exited  File System Check on /dev/disk/by-uuid/39DC-D48A                             
  systemd-fsckd.service                                 loaded    inactive dead    File System Check Daemon to report status                                    
  systemd-hwdb-update.service                           loaded    inactive dead    Rebuild Hardware Database                                                    
  systemd-initctl.service                               loaded    inactive dead    initctl Compatibility Daemon                                                 
  systemd-journal-flush.service                         loaded    active   exited  Flush Journal to Persistent Storage                                          
  systemd-journald.service                              loaded    active   running Journal Service                                                              
  systemd-logind.service                                loaded    active   running Login Service                                                                
  systemd-machine-id-commit.service                     loaded    active   exited  Commit a transient machine-id on disk                                        
  systemd-modules-load.service                          loaded    active   exited  Load Kernel Modules                                                          
  systemd-networkd-wait-online.service                  loaded    active   exited  Wait for Network to be Configured                                            
  systemd-networkd.service                              loaded    active   running Network Service                                                              
  systemd-pstore.service                                loaded    inactive dead    Platform Persistent Storage Archival                                         
  systemd-quotacheck.service                            loaded    inactive dead    File System Quota Check                                                      
  systemd-random-seed.service                           loaded    active   exited  Load/Save Random Seed                                                        
  systemd-remount-fs.service                            loaded    active   exited  Remount Root and Kernel File Systems                                         
  systemd-resolved.service                              loaded    active   running Network Name Resolution                                                      
  systemd-rfkill.service                                loaded    inactive dead    Load/Save RF Kill Switch Status                                              
  systemd-sysctl.service                                loaded    active   exited  Apply Kernel Variables                                                       
  systemd-sysusers.service                              loaded    active   exited  Create System Users                                                          
  systemd-timesyncd.service                             loaded    active   running Network Time Synchronization                                                 
  systemd-tmpfiles-clean.service                        loaded    inactive dead    Cleanup of Temporary Directories                                             
  systemd-tmpfiles-setup-dev.service                    loaded    active   exited  Create Static Device Nodes in /dev                                           
  systemd-tmpfiles-setup.service                        loaded    active   exited  Create Volatile Files and Directories                                        
  systemd-udev-settle.service                           loaded    active   exited  udev Wait for Complete Device Initialization                                 
  systemd-udev-trigger.service                          loaded    active   exited  udev Coldplug all Devices                                                    
  systemd-udevd.service                                 loaded    active   running udev Kernel Device Manager                                                   
● systemd-update-done.service                           not-found inactive dead    systemd-update-done.service                                                  
  systemd-update-utmp-runlevel.service                  loaded    inactive dead    Update UTMP about System Runlevel Changes                                    
  systemd-update-utmp.service                           loaded    active   exited  Update UTMP about System Boot/Shutdown                                       
  systemd-user-sessions.service                         loaded    active   exited  Permit User Sessions                                                         
● systemd-vconsole-setup.service                        not-found inactive dead    systemd-vconsole-setup.service                                               
● ua-auto-attach.service                                not-found inactive dead    ua-auto-attach.service                                                       
  ua-license-check.service                              loaded    inactive dead    Poll for Ubuntu Pro licenses (Only enabled on GCP LTS non-pro)               
  ua-reboot-cmds.service                                loaded    inactive dead    Ubuntu Advantage reboot cmds                                                 
  ua-timer.service                                      loaded    inactive dead    Ubuntu Advantage Timer for running repeated jobs                             
  udisks2.service                                       loaded    active   running Disk Manager                                                                 
  ufw.service                                           loaded    active   exited  Uncomplicated firewall                                                       
  unattended-upgrades.service                           loaded    active   running Unattended Upgrades Shutdown                                                 
  user-runtime-dir@0.service                            loaded    active   exited  User Runtime Directory /run/user/0                                           
  user@0.service                                        loaded    active   running User Manager for UID 0                                                       
  uuidd.service                                         loaded    inactive dead    Daemon for generating UUIDs                                                  
  vgauth.service                                        loaded    inactive dead    Authentication service for virtual machines hosted on VMware                 
● whoopsie.service                                      not-found inactive dead    whoopsie.service                                                             


129 loaded units listed.
To show all installed unit files use 'systemctl list-unit-files'.
