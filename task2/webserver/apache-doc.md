# Install webserver:

## Installing apache2
- Update Ubuntu to the latest version:
`sudo apt upgrade`

- Download apache2 and install it:
`sudo apt install apache2`

- Check apache2 status:
`systemctl status apache2`

- Setting up Firewall:
`ufw allow 'Apache'
ufw enable
ufw status`
- Enable SSH connection:
`ufw allow 'OpenSSH'`

- Firewall disabled for good:
`ufw disable`

---

## Serve a static page:
- Set up doman directory and index file:
`mkdir /var/www/stefko.me
vim /var/www/stefko.me/index.html`
- Set up config file for the domain:
`vim /etc/apache2/sites/available/stefko.me.conf`
- Set this file as default:
`a2ensite stefko.me.conf`
- Disable the previously default file:
`a2dissite 000-default.conf`
- Test if changes are applied and OK:
`apache2ctl configtest`
- Restart apache for changes to take place:
`systemctl restart apache2`
- Test if it's accessible:
> http://168.119.180.103/index.html

---

## Enable different port than 80 or 443

- Enable port 6969 in 'ports.config':
`vim /etc/apache2/ports.conf`
> Listen 6969
- Enable port 6969 in the default site config 'stefko.me.conf':
`vim /etc/apache2/sites-available/stefko.me.conf`
> <VirtualHost *:6969>
- Test if it's working:
> http://168.119.180.103:6969/index.html
