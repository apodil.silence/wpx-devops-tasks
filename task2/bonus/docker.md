# Bonus: Install Docker on the machine and run a containerized web server

- Install docker
`apt-get install docker.io`
- Download Apache image
`docker pull httpd`
- Check images
`docker images`
- Run the container on port 420
`docker run -d --name apache-server -p 420:420 httpd`
