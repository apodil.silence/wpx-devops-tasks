## Bonus: Configure the SSH server of the machine

- Change the port
`vim /etc/ssh/sshd_config`
> Port 2222
`service ssh restart`
- Generate ssh key
`ssh-keygen -t rsa -b 4096 -C "apodil.silence@gmail.com"`
- Copy the key to the VM
`ssh-copy-id root@168.119.180.103`
- Test if everything is working
`ssh root@168.119.180.103 -p 2222`
