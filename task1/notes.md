# Git

## Installing Git 

`git config --global user.name "Stefan Petrov"`
`git config --global user.email "apodil.silence@gmail.com"`

## Create git repo

`git init`

## Created an account with GitLab
 - Created new project "WPX DevOps Tasks"
 - Clone with SSH
`git@gitlab.com:apodil.silence/wpx-devops-tasks.git`
 - Clone with HTTPS 
>https://gitlab.com/apodil.silence/wpx-devops-tasks.git
 
`git remote add origin https://gitlab.com/apodil.silence/wpx-devops-tasks.git`
  
## Testing

 - Testing git commands
  
 - Tesetting git commits, since for testing purpose they had no valuable information nor changes

## Making directories to store notes and documents

`mkdir task0 task1 task2 task3`
