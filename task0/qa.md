# What is Git and how does it work?

Git is a software that tracks the changes you make on files and you can revert back to a previous version of a file. Git can be used locally or online, which makes it great for collaborations. Multiple people can work on a project, even on the same file at the same time and merge that file later into one.

How does it work - in Git you create your repo locally or clone a remote repository. On remote repo you can have your own branch where you can work on the project files and then commit and push the changes to the master (where all files of the project are stored in finished version). You also have the option to stage (add) only one file, of all the files you've worked on, for commit. You also have the option to merge two branches, merging two branches can have conflicts, if two developers both work on the same part of the file. Git have an option to show the two parts, so you can decide which one to keep.

# What is Docker and how does it work?

Docker is a software with which you can create and manage containers on top of a OS. It's like VM, but Dockers conteiners run on the same OS. Docker is effcient, beacouse it's lightweight and can take more workload on the same hardware.

 Doker daemon (dockerd) is the Engine of Docker, which is reponsible for all the actions related to containers. 
 
 Docker containerd (containerd) is a system responsible for downloading images* and running them as a container. 
  *Image is the building block of Docker, which can have prebuild application dependancies. They are stored in a Docker registry, which can be public or private. 

 Doker runc (runc) is the container run time, it's running the container commands. 

 Dockerfile - used for building the image. Text file that contains commands, one command per line. 

Dockerd receives commands from the Docker client through Rest API or CLI. Dockerd then checks the request, which then goes through the Registry/Image and then to the container.

# Describe the CAP theorem?

The three parts of CAP theorem are Consistency, Availability and Partition tolerance. The theorem states it's impossible to guarantee all three of those in a distributed system.
  
# Describe the perfect application development to deployment process;  

There are two models - 4 step and 3 step. The 3 step is prefered, since it's faster for deployment and less expensive. The steps in the 3-tier model are: development > staging > prodyction - in this model the step where the QA test the code before goes to staging is skipped. The issue with the 4 step module is as follows: the longer you wait for the code to be merge for testing, the harde it is to test. On other hand, the tested thing can change everytime new code is merged. All this process slows production.

Solution for issues with the 3 step process are Continious Integration (CI) and Continuous Deployment (CD). This is best applied through containers and managed with Git (branches make it easier), where every branch is a container based enviorment.

# Describe the perfect way that you would monitor infrastructure and applications;

  - Set up detailed alerts for each notification and priorotize them by urgency.
  - Test runs to check if everything runs okay and shows the appropriate alert in the system.
  - Be prepared for the worst case scenario and have possible solutions to minimize damages. (like second IPS in case SteadFast fuck up)
  - Check performance metrics on ragular basis, to ensure everything is running smoothly.
  - It's also important to have enough trained people to help keep track of everything, while they should be assigned by roles and monitor their specialization (security, hardware, software etc)
  
# Describe the perfect documentation and documentation processes;

  - Create a template for the documentation. It's best to create it to meet the team needs.
  - Record team meetings, important chat logs - they can help with the faundation of the document.
  - Establish a wiki, which will be accessibl by all of the team that needs this information.
  - Prioritize functionality over structior = okay structior and high functionality.
  
# What are the different phases in DevOps?
 
  - Continuous development - planning and coding
  - Continuous integration - commit changes regularly 
  - Continuous testing     - continuously tested for bugs
  - Continuous deployment  - deploy changes on regular basis
  - Continuous monitoring  - monitor the performance continuously

# Describe the perfect automated-application testing approach;

# Find some of the best container security image scanning tools and describe some of their functionality.

Docker Bench for Security - scan iclude vulnerabillities realted to the host config, dockerd config and files, container images and runtime, docker security operations and docker swarm config.
Dive - show basic layer info and an experimental metric to guess how much space is wasted in the containered image. 
Inspec - verifying the state of docker images against a security or compliance baseline.
Trivy - scan filesystems and git repos and brings list with details information, list of vulnerabillities.

# How would you enable network file sharing between two servers in two different regions?

The best way would be to set up a file sharing server. From what I see, most used one is Samba. It seems to be prefered because of the OS compatability (it can be used on linux, mac, windows). 

It has to be downloaded. Then setup it's configuration file. After that create a directory for sharing and setup the permissions for it. Restart the server and it should be working.
